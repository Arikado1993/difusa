<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_summary_cart{
    
    public function form_fields(){
        //1: campo requerido
        $send = array(
            'nombre_cliente' => 1,
			'apellido_paterno_cliente' => 1,
			'apellido_materno_cliente' => 1,
			'telefono_cliente' => 1,
			'celular_cliente' => 1,
			'email_cliente' => 1,
			'razon_social_factura' => 0,
			'calle_factura' => 0,
			'num_exterior_factura' => 0,
			'num_interior_factura' => 0,
			'colonia_factura' => 0,
			'localidad_factura' => 0,
			'delegacion_factura' => 0,
			'estado_factura' => 0,
			'cp_factura' => 0,
			'rfc_factura' => 0,
			'email_factura' => 0,
			'nombre_envio' => 1,
			'calle_envio' => 1,
			'num_exterior_envio' => 1,
			'num_interior_envio' => 0,
			'colonia_envio' => 1,
			'localidad_envio' => 1,
			'delegacion_envio' => 1,
			'estado_envio' => 1,
			'cp_envio' => 1,
			'telefono_1_envio' => 1,
			'telefono_2_envio' => 0,
			'referencia_envio' => 0,
			'descripcion_envio' => 0,
			'payment_option' => 1,
			'terms' => 1
        );
        return $send;
    }
    
    function shipping_fields(){
		$send = array(
			'address',
			'muni',
			'colony',
			'zip',
			'city',
			'state',
			'num_exterior',
			'num_interior'
		);
		return $send;
    }
    
    private function get_post($data){
        $CI = &get_instance();
		foreach($data as $key => $value){
			$send[$key] = $CI->input->post($key);
		}
		return $send;
    }
    
    public function validate_fields($data){
        $required_fields = true;
        foreach($this->form_fields() as $key => $value){
            if($value == 1){
                if(!$data[$key]){
                    $required_fields = false;
                }
            }
        }
        return $required_fields;
    }
    
    function get_amounts(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $iva = $CI->md_pages->get_option('iva');
        $iva = (float)$iva;
        $subtotal = 0;
        foreach($CI->cart->contents() as $value){
            $subtotal = $subtotal + $value['subtotal'];
        }
        $iva = ($subtotal / 100) * $iva;
        $send = array(
            'iva' => $iva,
            'subtotal' => $subtotal,
            'total' => $iva + $subtotal
        );
        return $send;
        
    }
    
    function create_paypal_data($data){
		/*
		https://www.paypal.com/cgi-bin/webscr?cmd=p/pdn/howto_checkout-outside
		https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/
                  //ndp 20150716 - en español
                https://www.paypal.com/cgi-bin/webscr?cmd=p/pdn/howto_checkout-outside#morevariables
		custom -> 256 Character Length
		invoice -> 127 Character Length
		*/
		$CI = &get_instance();
		$email_paypal = get_option('email_paypal');
		if($email_paypal){
			if($CI->cart->total_items() > 0){
				$time_send = str_replace(':','',str_replace(' ','',str_replace('-','',$data['time'])));
				$retun = base_url().'gracias-por-su-compra?t='.$time_send.'&f='.$data['folio'];
				$send = array(
					'cmd' => '_ext-enter',
					'redirect_cmd' => '_cart',
					'upload' => 1,
					'business' => $email_paypal,
					'currency_code' => 'MXN',
					'charset' => 'utf-8',
					'email' => $data['email_cliente'],
					'first_name' => $data['nombre_cliente'],
					'last_name' => $data['apellido_paterno_cliente'].' '.$data['apellido_materno_cliente'],
					'address1' => $data['calle_factura'].' '.$data['num_exterior_factura'].' '.$data['num_interior_factura'],
					'address2' => $data['colonia_factura'],
					'city' => $data['localidad_factura'].' '.$data['delegacion_factura'],
					'state' => $data['estado_factura'],
					'zip' => $data['cp_factura'],
					'H_PhoneNumber' => $data['telefono_cliente'],
					'return' => $retun,
					'cancel_return' => base_url().'carrito-de-compra',
					'custom' => $data['folio'],
					'invoice' => $time_send
				);
				$CI->load->model('md_pages');
				$num_items = 1;
				foreach($CI->cart->contents() as $value){
					$send['item_name_'.$num_items] = $value['name'];
					$send['item_number_'.$num_items] = $value['id'];
					$send['amount_'.$num_items] = format_number($value['price']);
					$send['quantity_'.$num_items] = $value['qty'];
					$send['on0_'.$num_items] = 'Modelo';
					$send['os0_'.$num_items] = $value['code'];
					$send['on1_'.$num_items] = 'Detalles';
					$send['os1_'.$num_items] = $value['name'].'/'.$value['code'];
					$num_items++;
				}
				
				$amounts = $this->get_amounts();
				
				$send['item_name_'.$num_items] = 'IVA';
				$send['amount_'.$num_items] = format_number($amounts['iva']);
				$send['quantity_'.$num_items] = 1;
				
				return $send;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
    }
    //NDP 20150715 - FUNCION ORIGINAL CON BOTON PARA PRODUCCION
	/*
	function create_bancomer_data($data = false){
		$CI = &get_instance();
		if($CI->cart->total_items() > 0){
			$retun = 'http://www.klamore.com.mx/gracias-por-su-compra';
			$send = array(
				'urlretorno' => $retun,
				'idexpress' => '339',
				'financiamiento' => '1',
				'plazos' => '6|3',
				'mediospago' => '100000',
				'referencia' => $data['folio']
			);
			$importe = 0;
			foreach($CI->cart->contents() as $value){
				$importe = ($value['price']*$value['qty']) + $importe;
			}
			$send['importe'] = $importe;
			return $send;
		}
		else{
			return false;
		}
	}*/
	
	//ndp 20150715 - boton para regreso a boton de klamore.com.mx/pruebas	
	function create_bancomer_data($data = false){
		$CI = &get_instance();
		if($CI->cart->total_items() > 0){
                        //PRODUCCION
			$retun = 'http://www.klamore.com.mx/pruebas/gracias-por-su-compra';
                        //TEST
                        //$retun = 'http://localhost/klamore.com.mx2/gracias-por-su-compra';
			$send = array(
				'urlretorno' => $retun,
				'idexpress' => '405',
				'financiamiento' => '1',
				'plazos' => '6|3|9',
				'mediospago' => '100000',
				'referencia' => $data['folio']
			);
			$importe = 0;
			foreach($CI->cart->contents() as $value){
				$importe = ($value['price']*$value['qty']) + $importe;
			}
			$send['importe'] = $importe;
			return $send;
		}
		else{
			return false;
		}
	}
	
    function create_session_for_payment_data($data){
		$CI = &get_instance();
		if($CI->session->userdata('payment_data')){
			$CI->session->unset_userdata('payment_data');
		}
		$CI->session->set_userdata('payment_data', json_encode($data));
		
		if($CI->session->userdata('paypal_data')){
			$CI->session->unset_userdata('paypal_data');
		}
		
		if($CI->session->userdata('bancomer_data')){
			$CI->session->unset_userdata('bancomer_data');
		}
		
		if($data['payment_option'] == 'paypal'){
			$paypal_data = $this->create_paypal_data($data);
			$CI->session->set_userdata('paypal_data', json_encode($paypal_data));
		}
		else{
			if($data['payment_option'] == 'bancomer'){
				$bancomer_data = $this->create_bancomer_data($data);
				$CI->session->set_userdata('bancomer_data', json_encode($bancomer_data));
			}
		}
	
    }
    
    
    function get_variables(){
        $CI = &get_instance();
        $CI->load->library('carousel');
        $send = array(
            'carousel_config' => $CI->carousel->summary_cart()
        );
        return $send;
    }
    
    function create_shipping_data($data){
        if($data['active_shipping']){
			foreach($this->shipping_fields() as $value){
				$data[$value.'_shipping'] = $data[$value];
			}
        }
        else{
            foreach($this->shipping_fields() as $value){
				$all_fields = true;
				if(!$data[$value.'_shipping']){
					$all_fields = false;
				}
			}
			if(!$all_fields){
				$this->missing_data();
			}
        }
        return $data;
    }
    
    function missing_data(){
        redirect('/carrito-de-compra');
    }
	
	public function create_boton_for_payment($data){
		$paymen_option = $data['payment_option'];
		$send = false;
		if($paymen_option == 'bancomer'){
			$send = $this->create_bancomer_data($data);
		}
		if($paymen_option == 'paypal'){
			$send = $this->create_paypal_data($data);
		}
                //ndp 20150730 - para pruebas
                if($paymen_option == 'pruebas'){
			$send = $this->create_bancomer_data($data);
		}
		return $send;
	}
	
    public function get_content(){
		$data = $this->get_post($this->form_fields());
		if($this->validate_fields($data)){
			$CI = &get_instance();
			$CI->load->model('md_invoicing');

			$data = $CI->md_invoicing->save_payment($data);
			$send = $this->get_variables();
			$send['data'] = $data;
                        
                        //ndp 20150730 - boton para pruebas, subir el folio a la sesion
                        log_message('debug','lb_summary_cart.get_content().$data = '.print_r($data,TRUE));
                        //SESSION
                        $CI->load->library('session');
                        
                        $CI->session->set_userdata('folio',$data['folio']);

                        /*
                         //pruebas
                        $session_user = $CI->session->all_userdata();
                        //$temp = $CI->session->__get('folio');
                        log_message('debug','$session_user[folio]; = '.print_r($session_user['folio'],TRUE));
                        */


			$send['boton_payment'] = $this->create_boton_for_payment($data);
			//pre($send);
			return $send;
		}
		else{
			$this->missing_data();
		}
    }
}