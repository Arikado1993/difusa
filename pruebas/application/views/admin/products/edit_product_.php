<div class="row-fluid">
    <div class="span12">
        <?php if($product_error == 'required'):  ?>
            <div class="alert alert-error">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong>Por favor.</strong> Los campos con (*) son necesario.
            </div>
        <?php endif; ?>
        <?php if($product_success): ?>
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close" type="button">×</button>
                El producto se edito correctamente.
            </div>
        <?php endif; ?>
        <form id = "edit_product_form" class="form-horizontal" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>products/edit" enctype="multipart/form-data">
            <table class="table table-striped">
                <tbody>
                    <?php if(isset($product_fields)):
                        foreach($product_fields as $key => $value): ?>
                            <?php if($key === 'currency'): ?>
                                <tr>
                                    <td>
                                        <label for="<?php echo $key; ?>">
                                            <?php if($required_products[$key] == 1)echo '* '.$value.':'; else echo $value.':';?>
                                        </label>
                                    </td>
                                    <td>
                                        <select id="<?php echo $key; ?>" name="<?php echo $key; ?>">
                                            <option value="MXN" <?php if(isset($info_product[$key]) and $info_product[$key] == 'MXN') echo 'selected'; ?>>MXN</option>
                                            <option value="USD" <?php if(isset($info_product[$key]) and $info_product[$key] == 'USD') echo 'selected'; ?>>USD</option>
                                        </select>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <?php if($key === 'description'): ?>
                                    <tr>
                                        <td>
                                            <label for="<?php echo $key; ?>">
                                                <?php if($required_products[$key] == 1)echo '* '.$value.':'; else echo $value.':';?>
                                            </label>
                                        </td>
                                        <td>
                                            <textarea class="editme textarea_description" id="<?php echo $key; ?>" name="<?php echo $key; ?>">
                                                <?php  if (isset($info_product[$key]))echo $info_product[$key]; ?>
                                            </textarea>
                                        </td>
                                    </tr>
                                <?php else: ?>
                                    <?php if($key === 'promotion'): ?>
                                        <tr>
                                            <td>
                                                <label for="<?php echo $key; ?>">
                                                    <?php if($required_products[$key] == 1)echo '* '.$value.':'; else echo $value.':';?>
                                                </label>
                                            </td>
                                            <td><input type="checkbox" id="<?php echo $key; ?>" name="<?php echo $key; ?>" <?php if(isset($info_product[$key]) and $info_product[$key] == 1) echo 'checked'; ?>></td>
                                        </tr>
                                    <?php else: ?>
                                        <?php if($key === 'video' ){ ?>
										<tr>
											<td><label for="pdf_product">Video:</label></td>
											<td><textarea name="video" id="video" class="input-xxlarge"><?php echo ($info_product[$key]);?></textarea></td>
										</tr>
										<?php } else {?>
										<tr>
                                            <td>
                                                <label for="<?php echo $key; ?>">
                                                    <?php if($required_products[$key] == 1)echo '* '.$value.':'; else echo $value.':';?>
                                                </label>
                                            </td>
                                            <td><input class="input-xxlarge" type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php  if (isset($info_product[$key]))echo $info_product[$key]; ?>"></td>
                                        </tr>
										<?php } ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach;
                    endif; ?>
                    <tr id="imagenes">
                        <td><label for="img_product">Imagen:</label></td>
                        <td>
                            <input type="file" id="img_product" name="img_product[]" multiple />
			    <?php $gallery = get_the_gallery($info_product['id']); ?>
			    <?php $url = get_bloginfo('template_url');?>
                            <div>
                                <?php foreach ($gallery as $img): ?>
                                    <a href="<?php echo base_url(); ?>products/delete/<?php echo $img->record_id;?>" class="link-remove-img-pro">
                                        <i class="icon-remove"></i>
                                    </a>
                                    <img src="<?php echo $url; ?>/timthumb/timthumb.php?src=<?php echo site_url();?>content/<?php echo get_option('images_dir');?><?php echo $img->url; ?>&amp;h=75&amp;w=75&amp;zc=2" alt="" />
				<?php endforeach; ?>
			    </div>
			</td>
                    </tr>
                    <tr id="documentos">
                        <td><label for="pdf_product">Hoja técnica:</label></td>
                        <td>
                            <input type="file" id="pdf_product" name="pdf_product[]" multiple />
                            <?php if($documents_of_product): ?>
                            <div>
                                <?php foreach($documents_of_product as $document): ?>
                                    <a href="<?php echo base_url(); ?>products/delete/<?php echo $document->id_product_meta;?>/document" class="link-remove-img-pro">
                                        <i class="icon-remove"></i>
                                    </a>
                                    <a href="<?php echo base_url(); ?>content/uploads/files/articulo/hoja_tecnica/<?php echo $document->meta_value; ?>" target="_blank" class="link-remove-img-pro">
                                        <img src="<?php echo $url; ?>/timthumb/timthumb.php?src=<?php echo site_url();?>content/themes/equus/img/iconos/file_icon.png&amp;h=75&amp;w=75&amp;zc=2" alt="documento" />
                                    </a>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>
                        </td>
                    </tr>
					
                    <tr>
                        <td><label>* Marcas:</label></td>
                        <td>
                            <select id="manufactures_option" name="manufactures_option[]" multiple>
                                <?php if(isset($all_manufactures)):
                                    foreach($all_manufactures as $value):?>
                                        <option value="<?php echo $value->manufacturer_id; ?>" <?php if(isset($manufacturers_of_product[$value->manufacturer_id])) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                    <?php endforeach;
                                endif;?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>* Categorías:</label></td>
                        <td>
                            <select id="categories_option" name="categories_option[]" multiple>
                                <?php if(isset($all_categories)):
                                    foreach($all_categories as $value):?>
                                        <option value="<?php echo $value->cat_id; ?>" <?php if(isset($categories_of_product[$value->cat_id])) echo 'selected'; ?>><?php echo $value->name; ?></option>
                                    <?php endforeach;
                                endif;?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-actions">
                <input type="hidden" name="edit_product" value="<?php echo $info_product['id']; ?>">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a class="btn" href="<?php echo base_url(); ?>admin/products">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<?php $this->load->view('admin/products/footer_form'); ?>