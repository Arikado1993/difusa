<?php get_header(); ?>
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />
<?php load_view('section_title', array('text' => 'Resumen de información')); ?>
<br />
<p class="text-summary" style="text-align: center;">
  Por favor verifique que toda su información es correcta incluyendo su información de envio.
  
  </p>
<br />
<?php
if(isset($data)):
if($data):
?>
<div class="container">
    
    <!-- I-datos del cliente -->
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">Datos del Cliente</h1>
		
        </div>
    </div>
    <br />
    <div class="row container-summary">
        <div class="col-md-12 col-md-offset-0  color-container-summary">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <br />
                    <p class="text-summary">
                        <span class="text-summary-title">Nombre completo:</span> <?php echo $data['nombre_cliente'].' '.$data['apellido_paterno_cliente'].' '.$data['apellido_materno_cliente']; ?>
                        <br />
                        <span class="text-summary-title">Teléfono:</span> <?php echo $data['telefono_cliente']; ?>
                        <br />
                        <span class="text-summary-title">Celular:</span> <?php echo $data['celular_cliente']; ?>
                        <br />
                        <span class="text-summary-title">Correo Electrónico:</span> <?php echo $data['email_cliente']; ?>
                    </p>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <!-- F-datos del cliente -->
    <?php if($data['razon_social_factura'] and $data['rfc_factura']): ?>
    <!-- I-datos de facturación -->
    <br />
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">Datos de Facturación</h1>
        </div>
    </div>
    <br />
    <div class="row container-summary">
        <div class="col-md-12 col-md-offset-0 color-container-summary">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <br />
                    <p class="text-summary">
                        <span class="text-summary-title">Nombre o Razón Social:</span> <?php echo $data['razon_social_factura']; ?>
                        <br />
                        <span class="text-summary-title">Calle:</span> <?php echo $data['calle_factura']; ?>
                        <br />
                        <span class="text-summary-title">Número Exterior:</span> <?php echo $data['num_exterior_factura']; ?>
                        <br />
                        <span class="text-summary-title">Número Interior:</span> <?php echo $data['num_interior_factura']; ?>
                        <br />
                        <span class="text-summary-title">Colonia:</span> <?php echo $data['colonia_factura']; ?>
                        <br />
                        <span class="text-summary-title">Localidad:</span> <?php echo $data['localidad_factura']; ?>
                        <br />
                        <span class="text-summary-title">Delegación o Munucipio:</span> <?php echo $data['delegacion_factura']; ?>
                        <br />
                        <span class="text-summary-title">Estado:</span> <?php echo $data['estado_factura']; ?>
                        <br />
                        <span class="text-summary-title">Código Postal:</span> <?php echo $data['cp_factura']; ?>
                        <br />
                        <span class="text-summary-title">RFC:</span> <?php echo $data['rfc_factura']; ?>
                        <br />
                        <span class="text-summary-title">Email:</span> <?php echo $data['email_factura']; ?>
                    </p>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <!-- F-datos de facturación -->
    <?php endif; ?>
    <br />
    <!-- I-datos de envío -->
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">Datos de Envío</h1>
        </div>
    </div>
    <br />
    <div class="row container-summary">
        <div class="col-md-12 col-md-offset-0  color-container-summary">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <br />
                    <p class="text-summary">
                        <span class="text-summary-title">Nombre de la Persona que Recibe:</span> <?php echo $data['nombre_envio']; ?>
                        <br />
                        <span class="text-summary-title">Calle:</span> <?php echo $data['calle_envio']; ?>
                        <br />
                        <span class="text-summary-title">Número Exterior:</span> <?php echo $data['num_exterior_envio']; ?>
                        <br />
                        <span class="text-summary-title">Número Interior:</span> <?php echo $data['num_interior_envio']; ?>
                        <br />
                        <span class="text-summary-title">Colonia:</span> <?php echo $data['colonia_envio']; ?>
                        <br />
                        <span class="text-summary-title">Localidad:</span> <?php echo $data['localidad_envio']; ?>
                        <br />
                        <span class="text-summary-title">Delegación o Municipio:</span> <?php echo $data['delegacion_envio']; ?>
                        <br />
                        <span class="text-summary-title">Estado:</span> <?php echo $data['estado_envio']; ?>
                        <br />
                        <span class="text-summary-title">Código Postal:</span> <?php echo $data['cp_envio']; ?>
                        <br />
                        <span class="text-summary-title">Teléfono 1:</span> <?php echo $data['telefono_1_envio']; ?>
                        <br />
                        <span class="text-summary-title">Teléfono 2:</span> <?php echo $data['telefono_2_envio']; ?>
                        <br />
                        <span class="text-summary-title">Referencia de Localización:</span> <?php echo $data['referencia_envio']; ?>
                        <br />
                        <span class="text-summary-title">Descripción de condiciones:</span> <?php echo $data['descripcion_envio']; ?>
                    </p>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <!-- F-datos de envío -->
    <br />
    <!-- I-forma de pago -->
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">Forma de Pago</h1>
        </div>
    </div>
    <br />
    <div class="row container-summary">
        <div class="col-md-12 col-md-offset-0 color-container-summary">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <br />
                    <p class="text-summary text-summary-title">
                        <?php if($data['payment_option'] == 'paypal'){
                            echo 'PayPal';
                        }
                        else{
                            if($data['payment_option'] == 'bancomer'){
                                echo 'Multipagos';
                            }
                        }?>
                    </p>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <!-- F-forma de pago -->
    <br />
    <!-- I-botones de acción-->
    <div class="row">
        <div class="col-md-5 col-md-offset-7">
            <form id="form_cancel_payment" name="form_cancel_payment" method="post" action="<?php echo base_url();?>cancelar-compra">
                <button id="btn_cancelar_compra" type="submit" class="btn btn-cotizar btn-block" form="none">Modificar información</button>
            </form>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-5 col-md-offset-7">
            <?php if($data['payment_option'] == 'paypal'): ?>
                <form id="form_paypal" name="form_paypal" method="post" action="https://www.paypal.com/cgi-bin/webscr">
                    <?php foreach($boton_payment as $key => $value): ?>
                        <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
                    <?php endforeach; ?>
                    <button id="btn_comprar_final" class="btn btn-ordenar btn-block" form="form_paypal">Comprar</button>
                </form>
            <?php endif; ?>
            <?php if($data['payment_option'] == 'bancomer'): ?>
                <!-- ndp 20150728 - ambiente de desarrollo -->
               <form id="form_bancomer" name="form_bancomer" action="https://www.adquiramexico.com.mx:443/mExpress/pago/avanzado" method="post">
               
                    <?php foreach($boton_payment as $key => $value): ?>
                        <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
                    <?php endforeach; ?>
                    <button id="btn_comprar_final" class="btn btn-ordenar btn-block" form="form_bancomer">Comprar</button>
                </form>
            <?php endif; ?>

            <?php
            //ndp 20150730 - pruebas en produccion
            if($data['payment_option'] == 'pruebas'): ?>
                <!-- ndp 20150728 - ambiente de desarrollo -->
               <!--<form id="form_bancomer" name="form_bancomer" action="https://www.adquiramexico.com.mx:443/mExpress/pago/avanzado" method="post">-->
               <!-- TEST -->
               <!--<form id="form_bancomer" name="form_bancomer" action="http://localhost/klamore.com.mx2/gracias-por-su-compra" method="post">-->
               <!-- PRODUCCION -->
               <form id="form_bancomer" name="form_bancomer" action="http://www.klamore.com.mx/pruebas/gracias-por-su-compra" method="post">
                    <?php foreach($boton_payment as $key => $value): ?>
                        <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>" />
                    <?php endforeach; ?>
                    <button id="btn_comprar_final" class="btn btn-ordenar btn-block" form="form_bancomer">Comprar</button>
                </form>
            <?php endif; ?>
        </div>
    </div>
    <!-- F-botones de acción -->
</div>
<?php endif; ?>
<?php endif; ?>
<?php load_view('modals_for_summary_cart'); ?>
<br /><br />
<?php get_footer(); ?>