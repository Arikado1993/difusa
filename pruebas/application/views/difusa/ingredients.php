<?php get_header(); ?>
<?php load_view('carousel_top', $carousel_config); ?>
<br/><br/>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0 text-left">
			<p>
				<strong>Ingredientes</strong>
			</p>
			<p>
				Nos centramos en la alta calidad y de alto valor los ingredientes y productos, ofrecemos productos especializados para diferentes procesos.
			</p>
			<br/><br/>
		</div>
	</div>
	<div class="row ">
		<div class="col-md-4">
			<img src="<?php echo get_option('path_template'); ?>img/malta.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-8" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>search-products/ingredientes/malta" class="link-to-cart">
					Malta
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los nuevos prooductos que están en stock que acaban de agregarse al inventario de productos que se están ofreciendo a los clientes via online.
			</p>
        </div>
    </div>
	<div class="row ">
		<div class="col-md-4">
			<img src="<?php echo get_option('path_template'); ?>img/levadura.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-8" style="padding: 8px 8px 8px 8px;  height : 291px;">
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>search-products/ingredientes/levadura" class="link-to-cart">
					Levadura
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los nuevos prooductos que están en stock que acaban de agregarse al inventario de productos que se están ofreciendo a los clientes via online.
			</p>
        </div>
    </div>
	<div class="row ">
		<div class="col-md-4">
			<img src="<?php echo get_option('path_template'); ?>img/lupulo.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-8" style="padding: 8px 8px 8px 8px;  height : 291px;">
           
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>search-products/ingredientes/lupulo" class="link-to-cart">
					Lúpulo
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los nuevos prooductos que están en stock que acaban de agregarse al inventario de productos que se están ofreciendo a los clientes via online.
			</p>
        </div>
    </div>
	<div class="row ">
		<div class="col-md-4">
			<img src="<?php echo get_option('path_template'); ?>img/enzimas.jpg" class="hidden-xs hidden-sm" alt="www.difusa.com.mx" />
		</div>		
        <div class="col-md-8" style="padding: 8px 8px 8px 8px;  height : 291px;">
           
			<h2 class="title-aboutus-1 " >
				<a href="<?php echo base_url(); ?>search-products/ingredientes/enzimas" class="link-to-cart">
					Enzimas
				</a>
			</h2>
			<p class="text-aboutus-1">
				Liga hacia los nuevos prooductos que están en stock que acaban de agregarse al inventario de productos que se están ofreciendo a los clientes via online.
			</p>
        </div>
    </div>
</div>	

<br />
<?php get_footer(); ?>