<?php get_header();
$load = false;
$load_in_cate = false;
$load_session = $this->session->userdata('load_in_category');
if($load_session){
    if($load_session == 1){
        $load = true;
    }
    else{
        if($load_session == 2){
            $load_in_cate = true;
        }
    }
    $this->session->unset_userdata('load_in_category');
}
?>
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />
<div <?php if($load_in_cate): ?>id="load_in_title_section"<?php endif; ?> class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"><?php echo $text_category; ?></h1>
        </div>
    </div>
</div>
<br /><br />
<div class="container">
    <?php $num_products = count($products);
    for($i = 0; $i < $num_products; $i = $i + 4):
        $aux = $i + 4; ?>
        <?php if($i): ?>
            <div class="row hidden-xs">
                <div class="col-md-12 col-md-offset-0 line-section-products"></div>
            </div>
            <br class="hidden-xs" /><br class="hidden-xs" />
        <?php endif; ?>
        <div class="row">
            <?php for($j = $i; $j < $aux; $j++): if(isset($products[$j])): ?>
                <div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 container-product-in-cat">
                    <a href="<?php echo $products[$j]->link; ?>" class="link-img-carousel link-container-ring">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h5 class="text-center text-name-product-in-cate"><?php echo $products[$j]->product_name; ?></h5>
                        </div>
                    </div>
                    </a>
                    <br />
                    <div class="row">
                        <img src="<?php echo $products[$j]->path_img.$products[$j]->images[0]; ?>" alt="<?php echo $products[$j]->product_name; ?>" class="img-responsive center-img size-img-product-cate img-default-produ "/>
                        <?php load_view('carousel_products', array('product' => $products[$j])); ?>
                    </div>
                    <br />
                    <a href="<?php echo $products[$j]->link; ?>" class="link-img-carousel link-container-ring">
						<div class="row">
							<div class="col-md-12 col-md-offset-0">
								<h6 class="text-left text-name-product-in-cate">
									Nombre Producto:
									<br class="visible-sm" />
									<?php echo $products[$j]->product_name; ?>
									<br />
									Código Producto:
									<br class="visible-sm" />
									<?php echo $products[$j]->product_code; ?>
									<br />
									Precio:
									<br class="visible-sm" />
									<span class="text-price-in-cate">$<?php echo format_number($products[$j]->price); ?></span>
								</h6>
							</div>
						</div>
                    </a>
                </div>
                <div class="row visible-xs">
                    <div class="col-xs-8 col-xs-offset-2 line-section-products-xs"></div>
                </div>
            <?php endif; endfor; ?>
        </div>
        <br class="hidden-xs" /><br class="hidden-xs" />
    <?php endfor; ?>
    <?php if($number_of_products > 8): ?>
        <div class="row">
            <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-2 col-xs-offset-0 text-left">
                <a href="<?php echo $pagination['previous']; ?>" class="glyphicon glyphicon-chevron-left link-view-more-products" aria-hidden="true"></a>
            </div>
            <div class="text-center col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-8 col-xs-offset-0">
                <nav>
                    <ul class="pagination pagination-sm">
                        <?php foreach($pagination_numbers as $key => $value):
                        if($key == 1){
                            $link_page = 'href="'.$link_to_pagination.'"';
                        }
                        else{
                            $link_page = 'href="'.$link_to_pagination.'/'.$key.'"';
                        } ?>
                        <li <?php if($value == 1){ echo 'class="active"'; } ?>><a <?php if($value == 0){ echo $link_page; } ?>><?php echo $key; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-2 col-xs-offset-0 text-right">
                <a href="<?php echo $pagination['next']; ?>" class="glyphicon glyphicon-chevron-right link-view-more-products" aria-hidden="true"></a>
            </div>
        </div>
    <?php endif; ?>
</div>
<br /><br />
<?php
    get_footer();
?>