<div class="container">
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div class="modal fade" id="modal_add_cart">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body add-cart-modal-body color-add-cart-modal">
                            <div class="row padding-add-cart-modal-content">
                                <div class="col-md-6 col-md-offset-0">
                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-0">
                                            <img src="<?php echo get_option('path_template'); ?>img/bolsa_carrito.png" alt="bolsa del carrito" class="img-modal-cart" />
                                        </div>
                                        <div class="col-md-9 col-md-offset-0">
                                            <h4 class="text-title-add-cart-modal">
                                                Resumen.<br />
                                                Lista de productos
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-0">
                                    <img src="<?php echo get_option('path_template'); ?>img/logo-web.png" alt="Logo Klamore" class="img-responsive img-logo-modal-add-cart" />
                                </div>
                            </div>
                            <div class="row padding-add-cart-modal-content">
                                <div class="col-md-12 col-md-offset-0 line-add-cart-modal"></div>
                            </div>
                            <div class="row container-info-add-cart-modal top-container-info-add-cart">
                                <div class="col-md-4 col-md-offset-0">
                                    <h5 class="text-info-add-cart-modal"><?php echo $name_ring; ?><br />
                                    Modelo: <?php echo $model_ring; ?></h5>
                                </div>
                                <div class="col-md-4 col-md-offset-0">
                                    <h5 class="text-info-add-cart-modal">
                                        Precio:<br />
                                        <span id="the_price_now_modal">$<?php echo format_number($price_ring); ?></span>
                                    </h5>
                                </div>
                            </div>
                            <div class="row container-info-add-cart-modal">
                                <div class="col-md-6 col-md-offset-0">
                                    <button id = "btn_go_to_list" type="button" class="btn btn-go-to-cart-modal btn-block" data-dismiss="modal">Revisar mi lista</button>
                                    <br />
                                </div>
                                <div class="col-md-6 col-md-offset-0">
                                    <button id = "btn_go_to_category" type="button" class="btn btn-return-category-modal btn-block" data-dismiss="modal">Seguir seleccionando</button>
                                    <br />
                                </div>
                            </div>
                            <br />
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div><!-- /modal -->
</div>