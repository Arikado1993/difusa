<?php get_header(); ?>
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />
<?php load_view('section_title', array('text' => $title)); ?>
<br /><br />
<?php if(isset($products_of_the_cart)): ?>
    <div class="container">
        <!-- I-productos del carrito -->
        <?php $i = 0; foreach($products_of_the_cart as $value):
            $img = $value['options']['path_img'].$value['options']['img']; ?>
            <div class="row">
                <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                            <h5 class="">&nbsp;</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                         
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-md-offset-0 col-sm-1 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <h5 class="title-cart-table text-center">&nbsp;</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row color-1-cart-table container-cart-table">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            <a href="<?php echo base_url(); ?>remover-del-carrito/<?php echo $value['rowid']; ?>" class="link-remove-product-cart">
                                <img src="<?php echo get_option('path_template'); ?>img/tache.png" alt="Eliminar del carrito" class="img-responsive center-img img-delete-product" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <h5 class="title-cart-table text-center">Descripción</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row color-2-cart-table container-cart-table">
                        <br />
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            <p class="details-cart-table">
                                <?php echo $value['name']; ?><br />
                                Código producto: <?php echo $value['code']; ?><br />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <h5 class="title-cart-table text-center">Cantidad</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row color-1-cart-table container-cart-table">
                        <br />
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            <p class="details-cart-table text-center margin-detail-cart-table"><?php echo $value['qty']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <h5 class="title-cart-table text-center">Precio unitario</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row color-2-cart-table container-cart-table">
                        <br />
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            <p class="details-cart-table text-right margin-detail-cart-table">$<?php echo format_number($value['price']); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <?php if($i == 0): ?>
                    <div class="row hidden-xs">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <h5 class="title-cart-table text-center">Precio total</h5>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row color-1-cart-table container-cart-table">
                        <br />
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            <p class="details-cart-table text-right margin-detail-cart-table">$<?php echo format_number($value['subtotal']); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php $i++; endforeach; ?>
        <!-- F-productos del carrito -->
        <!-- I-cantidades de la compra -->
        <div class="row">
            <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-4 col-xs-offset-1 container-amounts-cart-table">
                <h5 class="details-cart-table text-right margin-amounts-cart-table">Total (sin envío)</h5>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-6 col-xs-offset-0 color-2-cart-table container-amounts-cart-table">
                <p class="details-cart-table text-right margin-amounts-cart-table">$<?php echo format_number($amounts['subtotal']); ?></p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-4 col-xs-offset-1 container-amounts-cart-table">
                <h5 class="details-cart-table text-right margin-amounts-cart-table">IVA</h5>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-6 col-xs-offset-0 color-1-cart-table container-amounts-cart-table">
                <p class="details-cart-table text-right margin-amounts-cart-table">$<?php echo format_number($amounts['iva']); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-8 col-sm-2 col-sm-offset-8 col-xs-4 col-xs-offset-1 container-amounts-cart-table">
                <h5 class="details-cart-table text-right margin-amounts-cart-table">Total (sin envío)</h5>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-6 col-xs-offset-0 color-2-cart-table container-amounts-cart-table">
                <p class="details-cart-table text-right margin-amounts-cart-table">$<?php echo format_number($amounts['total']); ?></p>
            </div>
        </div>
        
        <!-- F-cantidades de la compra -->
        <br />
        <!-- I-botones de acciÃ³n -->
        <?php /*
        <div class="row">
            <div class="col-md-5 col-md-offset-7">
                <button id="btn_cotizar" class="btn btn-cotizar btn-block">Generar CotizaciÃ³n</button>
            </div>
        </div>
        <br />
        */ ?>
        <div class="row">
            <div class="col-md-5 col-md-offset-7 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1">
                <button id="btn_ordenar" class="btn btn-ordenar btn-block">Ordenar</button>
            </div>
        </div>
        <!-- F-botones de acciÃ³n -->
        <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" /><br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" />
        <!-- I-contenedor de tiempo de entrega -->
        <div id="step_1_cart" class="row color-barra-seccion <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
            <div class="col-md-12 col-md-offset-0">
                <h1 class="text-center texto-barra-seccion">Tiempo de entrega</h1>
            </div>
        </div>
        <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" />
        <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
            <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                <p class="text-justify text-tiempo-entrega">
                    Después de haber sido confirmado su pedido, seria entregado en la puerta de su casa en un máximo
                    de 7 a 10 días garantizados, agradecemos su confianza.
                </p>
            </div>
        </div>
        <!-- F-contenedor de tiempo de entrega -->
        <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" /><br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" /><br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" />
        <!-- I-form-facturaciÃ³n -->
        <form id="form_facturacion" name="form_facturacion" method="post"  action="<?php echo base_url();?>resumen-de-la-compra">
            <input type='hidden' value='1' name='summary' />
            <?php if(isset($error)): ?>
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <p class="bg-danger">Por favor, debe de llenar los campos requeridos</p>
                </div>
            </div>
            <?php endif; ?>
            <!-- I-Datos de la persona -->
            <div <?php if($payment_data){ echo 'id="load_in_here"'; } ?> class="row color-barra-seccion <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
                <div class="col-md-12 col-md-offset-0">
                    <h1 class="text-center texto-barra-seccion">Datos del Cliente</h1>
                </div>
            </div>
            <!-- F-Datos de la persona -->
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <p class="text-justify text-tiempo-entrega">
                        Los campos son * son obligatorios.
                    </p>
                </div>
            </div>
            
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>" />
            
            <!-- I-Campos del cliente -->
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
                <!-- Nivel 1  -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="nombre_cliente">*Nombre:</label>
                        <input type="text" class="form-control control-contact" id="nombre_cliente" name="nombre_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['nombre_cliente'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="apellido_paterno_cliente">*Apellido Paterno:</label>
                        <input type="text" class="form-control control-contact" id="apellido_paterno_cliente" name="apellido_paterno_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['apellido_paterno_cliente'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="apellido_materno_cliente">*Apellido Materno:</label>
                        <input type="email" class="form-control control-contact" id="apellido_materno_cliente" name="apellido_materno_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['apellido_materno_cliente'].'"'; } ?> />
                    </div>
                </div>
            </div>
            
            
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
                
                <!-- Nivel 2  -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="telefono_cliente">*Teléfono:</label>
                        <input type="text" class="form-control control-contact" id="telefono_cliente" name="telefono_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['telefono_cliente'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="celular_cliente">*Teléfono Celular:</label>
                        <input type="text" class="form-control control-contact" id="celular_cliente" name="celular_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['celular_cliente'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="email_cliente">*Correo Electrónico:</label>
                        <input type="text" class="form-control control-contact" id="email_cliente" name="email_cliente" <?php if($payment_data) { echo 'value="'.$payment_data['email_cliente'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            <!-- F-Campos del cliente -->
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-1-cart'; } ?>">
                <div class="col-md-5 col-md-offset-7 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1">
                    <button id="btn_next_step" type="submit" class="btn btn-cotizar btn-block" form="none">Siguiente</button>
                </div>
            </div>
            
            <!-- I-titulo datos de facturaciÃ³n -->
            <div <?php if($payment_data){ echo 'id="load_in_here"'; } else{ echo 'id="step_2_cart"'; } ?> class="row color-barra-seccion <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                <div class="col-md-12 col-md-offset-0">
                    <h1 class="text-center texto-barra-seccion">Datos de Facturación</h1>
                </div>
            </div>
            <!-- F-titulo datos de facturaciÃ³n -->
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <p class="text-justify text-tiempo-entrega">
                        Los campos son * son obligatorios.
                    </p>
                </div>
            </div>
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>" />
            <!-- I-campos de facturaciÃ³n -->
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                <!-- Nivel 1  -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="razon_social_factura">*Nombre o Razón Social:</label>
                        <input type="text" class="form-control control-contact" id="razon_social_factura" name="razon_social_factura" <?php if($payment_data) { echo 'value="'.$payment_data['razon_social_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="calle_factura">*Calle:</label>
                        <input type="text" class="form-control control-contact" id="calle_factura" name="calle_factura" <?php if($payment_data) { echo 'value="'.$payment_data['calle_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="num_exterior_factura">*Número Exterior:</label>
                        <input type="email" class="form-control control-contact" id="num_exterior_factura" name="num_exterior_factura" <?php if($payment_data) { echo 'value="'.$payment_data['num_exterior_factura'].'"'; } ?> />
                    </div>
                </div>
            </div>
            
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                
                <!-- Nivel 2  -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="num_interior_factura">NÃºmero Interior:</label>
                        <input type="text" class="form-control control-contact" id="num_interior_factura" name="num_interior_factura" <?php if($payment_data) { echo 'value="'.$payment_data['num_interior_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="colonia_factura">*Colonia:</label>
                        <input type="text" class="form-control control-contact" id="colonia_factura" name="colonia_factura" <?php if($payment_data) { echo 'value="'.$payment_data['colonia_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="localidad_factura">*Localidad:</label>
                        <input type="text" class="form-control control-contact" id="localidad_factura" name="localidad_factura" <?php if($payment_data) { echo 'value="'.$payment_data['localidad_factura'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                
                <!-- Nivel 3  -->
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="delegacion_factura">*DelegaciÃ³n o Municipio:</label>
                        <input type="text" class="form-control control-contact" id="delegacion_factura" name="delegacion_factura" <?php if($payment_data) { echo 'value="'.$payment_data['delegacion_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="estado_factura">*Estado:</label>
                        <select class="form-control control-contact" id="estado_factura" name="estado_factura">
                            <option value=""></option>
                            <?php foreach(get_states() as $value): ?>
                                <option value="<?php echo $value; ?>" <?php if($payment_data) { if($payment_data['estado_factura'] == $value){ echo 'selected';  } } ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="cp_factura">*CÃ³digo Postal:</label>
                        <input type="text" class="form-control control-contact" id="cp_factura" name="cp_factura" <?php if($payment_data) { echo 'value="'.$payment_data['cp_factura'].'"'; } ?> />
                    </div>
                </div>
                
                
            </div>
            
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                <!-- Nivel 4 -->
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="rfc_factura">*RFC:</label>
                        <input type="text" class="form-control control-contact" id="rfc_factura" name="rfc_factura" <?php if($payment_data) { echo 'value="'.$payment_data['rfc_factura'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="email_factura">*Correo ElectrÃ³nico para enviÃ³ de la factura:</label>
                        <input type="text" class="form-control control-contact" id="email_factura" name="email_factura" <?php if($payment_data) { echo 'value="'.$payment_data['email_factura'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            <!-- F-campos de facturaciÃ³n -->
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-2-cart'; } ?>">
                <div class="col-md-5 col-md-offset-7 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1">
                    <button id="btn_next_step_2" type="submit" class="btn btn-cotizar btn-block" form="none">Siguiente</button>
                </div>
            </div>
            
            
            <div id="step_3_cart" class="row color-barra-seccion hidden-sections-cart step-3-cart">
                <div class="col-md-12 col-md-offset-0">
                    <h1 class="text-center texto-barra-seccion">Datos de Envío</h1>
                </div>
            </div>
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-3-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-3-cart'; } ?>">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <p class="text-justify text-tiempo-entrega">
                        Los campos son * son obligatorios.
                    </p>
                </div>
            </div>
            
            <!-- I-formulario para datos de envÃ­o -->
            <div class="row hidden-sections-cart step-3-cart">
                <br />
                <!-- Nivel 1 envio -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="nombre_envio">*Nombre de la Persona que Recibe:</label>
                        <input type="text" class="form-control control-contact" id="nombre_envio" name="nombre_envio" <?php if($payment_data) { echo 'value="'.$payment_data['nombre_envio'].'"'; } ?>/>
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="calle_envio">*Calle:</label>
                        <input type="text" class="form-control control-contact" id="calle_envio" name="calle_envio" <?php if($payment_data) { echo 'value="'.$payment_data['calle_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="num_exterior_envio">*Número exterior:</label>
                        <input type="text" class="form-control control-contact" id="num_exterior_envio" name="num_exterior_envio" <?php if($payment_data) { echo 'value="'.$payment_data['num_exterior_envio'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            <div class="row hidden-sections-cart step-3-cart">
                
                <!-- Nivel 2 envio -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="num_interior_envio">Número interior:</label>
                        <input type="text" class="form-control control-contact" id="num_interior_envio" name="num_interior_envio" <?php if($payment_data) { echo 'value="'.$payment_data['num_interior_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="colonia_envio">*Colonia :</label>
                        <input type="text" class="form-control control-contact" id="colonia_envio" name="colonia_envio" <?php if($payment_data) { echo 'value="'.$payment_data['colonia_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="localidad_envio">*Localidad:</label>
                        <input type="text" class="form-control control-contact" id="localidad_envio" name="localidad_envio" <?php if($payment_data) { echo 'value="'.$payment_data['localidad_envio'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            
            <div class="row hidden-sections-cart step-3-cart">
                <!-- Nivel 3 envio -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="delegacion_envio">*Delegación o Municipio:</label>
                        <input type="text" class="form-control control-contact" id="delegacion_envio" name="delegacion_envio" <?php if($payment_data) { echo 'value="'.$payment_data['delegacion_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="estado_envio">*Estado:</label>
                        <select class="form-control control-contact" id="estado_envio" name="estado_envio">
                            <option value=""></option>
                            <?php foreach(get_states() as $value): ?>
                                <option value="<?php echo $value; ?>" <?php if($payment_data) { if($payment_data['estado_envio'] == $value){ echo 'selected';  } } ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="cp_envio">*Código Postal:</label>
                        <input type="text" class="form-control control-contact" id="cp_envio" name="cp_envio" <?php if($payment_data) { echo 'value="'.$payment_data['cp_envio'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            
            <div class="row hidden-sections-cart step-3-cart">
                <!-- Nivel 4 envio -->
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="telefono_1_envio">*TelÃ©fono 1:</label>
                        <input type="text" class="form-control control-contact" id="telefono_1_envio" name="telefono_1_envio" <?php if($payment_data) { echo 'value="'.$payment_data['telefono_1_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="telefono_2_envio">TelÃ©fono 2:</label>
                        <input type="text" class="form-control control-contact" id="telefono_2_envio" name="telefono_2_envio" <?php if($payment_data) { echo 'value="'.$payment_data['telefono_2_envio'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            
            <div class="row hidden-sections-cart step-3-cart">
                <!-- Nivel 4 envio -->
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="referencia_envio">Referencia de LocalizaciÃ³n:</label>
                        <input type="text" class="form-control control-contact" id="referencia_envio" name="referencia_envio" <?php if($payment_data) { echo 'value="'.$payment_data['referencia_envio'].'"'; } ?> />
                    </div>
                </div>
                
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="form-group">
                        <label class="label-contact" for="descripcion_envio">DescripciÃ³n de condiciones:</label>
                        <input type="text" class="form-control control-contact" id="descripcion_envio" name="descripcion_envio" <?php if($payment_data) { echo 'value="'.$payment_data['descripcion_envio'].'"'; } ?> />
                    </div>
                </div>
                
            </div>
            
            <br class="<?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-3-cart'; } ?>" />
            <div class="row <?php if($payment_data) { echo 'step-1-cart-up'; } else { echo 'hidden-sections-cart step-3-cart'; } ?>">
                <div class="col-md-5 col-md-offset-7 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1">
                    <button id="btn_next_step_3" type="submit" class="btn btn-cotizar btn-block" form="none">Siguiente</button>
                </div>
            </div>
            
            <!-- F-formulario para datos de envÃ­o -->
            <div id="step_4_cart" class="row color-barra-seccion hidden-sections-cart step-4-cart">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                    <h1 class="text-center texto-barra-seccion">Forma de pago</h1>
                </div>
            </div>
            <br class="hidden-sections-cart step-4-cart" />
            <!-- I-opciones de pago -->
            <div class="row hidden-sections-cart step-4-cart">
                <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <div class="radio">
                        <label class="label-contact">
                            <input class="payment_option" type="radio" name="payment_option" value="paypal" checked/>
                            PayPal
                        </label>
                    </div>
                    <div class="radio">
                        <label class="label-contact">
                            <input class="payment_option" type="radio" name="payment_option" value="bancomer" />
                            Multipagos
                        </label>
                    </div>
                    <!-- ndp 20150730 - pruebas en produccion -->
                    <div class="radio">
                        <label class="label-contact">
                            <input class="payment_option" type="radio" name="payment_option" value="pruebas" />
                            Pruebas
                        </label>
                    </div>
                </div>
            </div>
            <!-- F-opciones de pago -->
            <br class="hidden-sections-cart step-4-cart" /><br class="hidden-sections-cart step-4-cart" />
            <div class="row hidden-sections-cart step-4-cart">
                <div class="col-md-7 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <label class="label-contact" for="terms">Acepto los tÃ©rminos y condiciones asociados a la venta de este producto</label>
                    <input name="terms"  id="terms" type="checkbox" <?php if($payment_data) { if($payment_data['terms']){ echo 'checked';  } } ?> />
                </div>
                <div class="col-md-5 col-md-offset-0 col-sm-5 col-sm-offset-7 col-xs-10 col-xs-offset-1">
                    <button id="btn_comprar" class="btn btn-ordenar btn-block" form="none">Comprar Ahora</button>
                </div>
            </div>
        </form>
        <!-- F-form-facturaciÃ³n -->
    </div>
<?php endif; ?>

<?php load_view('modals_for_cart'); ?>
<br /><br />
<?php get_footer(); ?>
<script src="<?php echo get_option('path_template'); ?>js/script.difusa.js"></script>