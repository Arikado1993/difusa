$(document).ready(function(){
	
	tinymce.init({
		mode : "specific_textareas",
		editor_selector: /(textarea_description)/
	});
	
	$('#edit_product_form').validate({
		rules :{
			name:{
				required : true
			},
			model:{
				required : true
			},
			slug:{
				required : true
			},
			price:{
				required : true
			}
		}
	});
	
	$.mask.definitions['s'] = "[0-9a-z-]";
	$.mask.definitions['n'] = "[0-9.]";
	
	$("#slug").mask("?ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss",{placeholder:""});
	$("#price").mask("?nnnnnnnnnnnnnnnnnnnnnnnnnnnnnn",{placeholder:""});
});