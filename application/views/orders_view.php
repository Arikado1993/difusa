<div class="form-group">
	<br><br><br>
	<legend>Pedidos</legend>
	<?php
		if(count($orders)>0)
		{
	?>
		<table class='rowstable'>
			<tr><th>Orden</th><th>Estatus de la orden</th><th>Fecha de orden</th><th>Confirmaci&oacute;n de pago</th></tr>
			<?php
				for($i=0;$i<count($orders);$i++)
				{
					echo "<tr>"; 
					echo "<td><a href='".base_url()."order-details'>".$orders[$i]['id_order']."</a></td>";
  					echo "<td>".$orders[$i]['order_status']."</td>";
	  				echo "<td>".$orders[$i]['order_date']."</td>";
  					echo "<td>".$orders[$i]['payment_confirmation_date']."</td>";
  					echo "</tr>";
				}
			?>
		</table>
	<?php
		}
		else echo "No tienes ninguna orden";
	?>
</div>
</div>
</div>
</div>
</div>
<?php get_footer();?>