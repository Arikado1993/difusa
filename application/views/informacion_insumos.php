<?php get_header();?>
<!--informacion de producto clickeado para usuarios sin cuenta (al público)-->
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />

<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"></h1>
        </div>
    </div>
</div>
<br /><br />
<div id="prodinfo">
<table class="inf">
<tr>
<th>Descripción</th>
<th>Producto</th>
</tr>
<tr>
<td class="opcion">
Nombre: <?php echo $info[0]['product_name'].'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';?>
Código: <?php echo $info[0]['product_code'];?><br><br>

<?php echo $info[0]['product_description'];?><br><br>

<?php echo $info[0]['category'];?>-><?php echo $info[0]['sub_category'].'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';?>
Marca: <?php echo $info[0]['brand'].'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';?>
País: <?php echo $info[0]['country_name'];?><br><br>
Precio: <?php echo $info[0]['price'].' ';?><?php echo $info[0]['currency_name'];?>->
Presentación: <?php echo $info[0]['presentation'].' ';?> <br/><br/><br/>

<a class="linkinfo" href="<?php echo base_url();echo $info[0]['url_techsheets'];?>">
<div class="bot">&nbsp&nbsp<img src="<?php echo base_url();?>img/ico_pdf.png"> Ficha Técnica&nbsp&nbsp</div>
</a>&nbsp&nbsp
<a  href="#popup1">
  <div class="bot">&nbsp&nbsp<img src="<?php echo base_url();?>img/cart.gif"/>
    Agregar al carrito de compras&nbsp&nbsp
    </div>
</a>
</td>
<td class="opcion">
<img id="imginfo" src="<?php echo base_url();echo $info[0]['url_images'];?>"/><br><br></td>
</tr>

</table>
</div>
<br><br><br><br><br><br><br>
<div class="overlay" id="popup1" >
  <div class="popup">
    <h3>Agregar al carrito</h3><br>
    <a class="close" href="#">&times;</a>
    <div class="content">
     <form action="<?php echo base_url(); ?>articulos_insumos_controller/agregarProducto"  method="post">
     <input type="hidden" name="idproduct" value="<?php echo  $info[0]['id_product']; ?>">
      Nombre: <?php echo  $info[0]['product_name']; ?>
      <input type="hidden" name="nombre" value="<?php echo  $info[0]['product_name']; ?>"><br>
      Código: <?php echo  $info[0]['product_code']; ?>
      <input type="hidden" name="codigo" value="<?php echo  $info[0]['product_code']; ?>"><br>
      Presentación: <?php echo  $info[0]['presentation']; ?>
      <input type="hidden" name="presentacion" value="<?php echo  $info[0]['presentation']; ?>"><br>
      Precio: <?php echo  $info[0]['price']; ?> <?php echo  $info[0]['currency_name']; ?>
      <input type="hidden" name="precio" value="<?php echo  $info[0]['price']; ?>">
      <input type="hidden" name="moneda" value="<?php echo  $info[0]['currency_name']; ?>"><br><br>
     Cantidad: <br><input type="number" name="cantidad" min="1" style="width:80px;">
            <br><br><input type="submit" class="botoncart" value="Agregar">
      </form>
  </div>
  </div>
</div>
<?php
    get_footer();
?>