<div class="container">
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_cancel_payment" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Desea cancelar la compra?</p>
                        </div>
                        <div class="modal-footer">
                            <button id="yes_cancel_payment" type="button" class="btn btn-default" data-dismiss="modal">Si</button>
                            <button id="no_cancel_payment" type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
</div>