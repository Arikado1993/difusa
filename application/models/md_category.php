<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_category extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database();
    }
 
    function categoria($category)
    {
       $query='SELECT s.sub_category, s.slug FROM ops_sub_categories s, ops_categories c WHERE s.id_category=c.id_category AND c.id_category="'.$category.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
}