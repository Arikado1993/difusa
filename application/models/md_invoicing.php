<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_invoicing extends CI_Model{

	public function __construct(){
		$this->load->database();
	}
	
	public function save_invoicing($data){
		//user id falta
		$this->db->trans_begin();
		
		$this->db->set('date', 'NOW()', FALSE);
		$this->db->insert('invoicing',$data);
		$id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}
		else{
			$this->db->trans_commit();
			return $id;
		}
	}
	
	public function get_promotion_by_code($promotion_code = 0){
		$this->db->select('
			id,
			id `promotion_id`,
			code,
			discount,
			description,
			activation,
			valid_date,
			enable
		');
		$this->db->from('promotion');
		$this->db->where('code', $promotion_code);
		$this->db->where('enable', '1');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d['promotion_id'] =  $row->id;
				$d['code'] =  $row->code;
				$d['discount'] =  $row->discount;
				$d['description'] =  $row->description;
				$d['activation'] =  $row->activation;
				$d['valid_date'] =  $row->valid_date;
				$d['enable'] =  $row->enable;
			}
		}
		return $d;
	}
	public function get_shipping_id($shipping_id = 0){
		$this->db->select('
			id,
			id `shipping_id`,
			description,
			key,
			price,
			activation,
			enable
		');
		$this->db->from('shipping');
		$this->db->where('id', $shipping_id);
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d['shipping_id'] =  $row->id;
				$d['description'] =  $row->description;
				$d['key'] =  $row->key;
				$d['price'] =  $row->price;
				$d['activation'] =  $row->activation;
				$d['enable'] =  $row->enable;
			}
		}
		return $d;
	}
        /**
         * ndp 20150821
         * @param <type> $folio
         */
        function get_informacion_pago($folio = 0){
            	$this->db->select('
                                    id_pago,
                                    informacion,
                                    folio
                                    ');
                $this->db->from('pagos');
                $this->db->where('id_pago', $folio);
                //$this->db->where('folio', $folio);

                $d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d['id_pago'] =  $row->id_pago;
				$d['informacion'] =  $row->informacion;
				$d['folio'] =  $row->folio;
			}
		}
		return $d;
        }

        
	function get_invoicing_by_email($email = false){
		
		$this->db->select('
			id,
			id `invoicing_id`,
			name,
			last_name,
			phone,
			trade_name,
			rfc,
			address,
			neighborhood,
			postal_code,
			city,
			state,
			email,
			phone_office,
			personal_email,
			postal_code_shipping,
			neighborhood_shipping,
			address_shipping,
			state_shipping,
			city_shipping
		');
		//$email = $this->db->escape($email);
		
		$this->db->from('invoicing');
		
		$this->db->where('email', $email);
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d =  $row;
			}
		}
		return $d;
	}
	
	function save_payment($data = false){
		if($data){
			$this->db->trans_begin();
			
			date_default_timezone_set('Mexico/General');
			$time = date("Y-m-d H:i:s");
			
			/*foreach($data['cart_data'] as $key => $value){
				$data['cart_data'][$key] =  (array) $value;
			}*/
			$session_user = $this->session->all_userdata();
			$data['session_user'] = $session_user;
			$data_json = json_encode($data);
			
			
			$insert = array(
				'fecha_registro' => $time,
				'metodo_pago' => $data['payment_option'],
				'informacion' => $data_json,
                                //ndp 20150728 - se agrego explicitamente el valor 0 cero para que no ha sido pagado.
                                'pagado' =>0
			);
			
			$this->db->insert('pagos', $insert);
			$id = $this->db->insert_id();
			
			$update_folio = array(
				'folio' => $id
			);
			$this->db->where('id_pago', $id);
			$this->db->update('pagos', $update_folio);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return 0;
			}
			else{
				$this->db->trans_commit();
				$data['time'] = $time;
				$data['folio'] = $id;
				$data['session_user'] = '';
				return $data;
			}
			
		}
	}
	
	function update_payment($data = false, $id_pago = 0){
	
	
		log_message('debug', '...update_payment($data = false, $id_pago = 0) =='.$id_pago);
		if($data){
			$this->db->trans_begin();
			
			date_default_timezone_set('Mexico/General');
			$time = date("Y-m-d H:i:s");
			
			$update = array(
				'pagado' => 1,
				'fecha_confirmacion' => $time,
				'respuesta_metodo_pago' => $data
			);
			$this->db->where('id_pago', $id_pago);
			$this->db->update('pagos', $update);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();
			}
			
		}
	}
	
	
	function save_no_payment($data = false){
		if($data){
			$this->db->trans_begin();
			
			date_default_timezone_set('Mexico/General');
			$time = date("Y-m-d H:i:s");
			
			
			$insert = array(
				'fecha_registro' => $time,
				'metodo_pago' => 'ind',
				'informacion' => $data,
				'folio' => 0
			);
			
			$this->db->insert('pagos', $insert);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();
			}
			
		}
	}
	
	
}