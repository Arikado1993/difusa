<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_productos extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function producto($prod)
    {
        $query='SELECT p.product_name, p.product_code, b.brand, p.price, c.currency_name, p.before_price FROM ops_products p, ops_currency c, ops_brands b, ops_sub_categories s WHERE p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND s.id_sub_category=p.id_sub_category AND s.id_sub_category="'.$prod.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    
}