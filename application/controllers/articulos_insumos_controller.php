<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articulos_insumos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_insumos');
   		$this->load->helper('text');
   		$this->load->library('Lb_carro');
	}

	function productos()
	{
		log_message('debug', 'md_insumos->producto($prod)');

			$prod = 'MAL';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0,
				 'pagina'=>1
			);
 			
 				
				$this->load->view('insumos_user',$send);
		
		log_message('debug', 'md_insumos->productos(INS).$this->db = '.print_r($this->db,TRUE));
	}
	
	function paginas($ini,$numero,$cant,$prod,$pagina)
	{
		
		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>$cant,
				 'numero'=>$numero,
				 'ini'=>$ini,
				 'pagina'=>$pagina
				 
			);

			
 					
				$this->load->view('insumos_user',$send);
	}

	function filtro()
	{

		if ((!isset($_POST['marca']))&&(!isset($_POST['presentacion']))&&(!isset($_POST['pais']))&&(!isset($_POST['cate']))) {
			$marca="%";
			$presen="%";
			$pais="%";
			$prod="%";
		}
		else{
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=8;
		}
		if ($canti<1){
			$canti=8;
		}

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->filtro($marca,$presen,$pais,$prod),
				'pais'=>$this->md_insumos->pais(),
				'cate'=>$this->md_insumos->catego(),
				'marca'=>$this->md_insumos->marca(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0,
				 'pagina'=>1
			);

			
 				
				$this->load->view('insumos_user',$send);


	}

	function agregarProducto()
    {
        $id = $_POST['idproduct'];
        $producto = $this->md_insumos->info($id);
        $cantidad = $_POST['cantidad'];
        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $codigo = $_POST['codigo'];
        $moneda_origen = $_POST['moneda'];
		$moneda_destino='MXN';

		$this->lb_carro->agregar($id,$cantidad,$precio,$nombre,$codigo);
        /*
		$get = file_get_contents("https://www.google.com/finance/converter?a=$precio2&from=$moneda_origen&to=$moneda_destino");
		$get = explode("<span class=bld>",$get);
		$get = explode("</span>",$get[1]);*/

     	
     	//$precio=preg_replace("/[^0-9\.]/", null, $get[0]);
        //obtenemos el contenido del carrito
       
        
        //redirigimos mostrando un mensaje con las sesiones flashdata
        //de codeigniter confirmando que hemos agregado el producto
        $this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
     
     
		$prod = 'MAL';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0,
				 'pagina'=>1
			);
 			
 				
				$this->load->view('insumos_user',$send);

    }
    
    function eliminarProducto($rowid) 
    {
    	$this->lb_carro->eliminarProd($rowid);
 		$this->load->view('carro');
    }
    
    function eliminarCarrito() {
        $this->lb_carro->eliminarCarro();
        $this->load->view('carro');
    }

    function carrito()
    {
    	
		$this->load->view('carro');
    }

    function insertWish()
    {
    	$producto = $this->input->post('idproduct',TRUE);
    	$user = $this->session->userdata('id_client');
    	$wishInsert = array(
    		'id_user' => $user,
    		'id_product' => $this->input->post('idproduct',TRUE)
    	);
    	$this->md_insumos->insertWish($wishInsert);
    	echo "<script language='javascript'>alert('Producto agregado correctamente');</script>";
    	$prod = 'MAL';
		$send = array(
			'carousel_config' => $this->carousel->productos(),
			'info'=>$this->md_insumos->productos($prod),
			'pais'=>$this->md_insumos->pais(),
			'marca'=>$this->md_insumos->marca(),
			'cate'=>$this->md_insumos->catego(),
			'presentacion'=>$this->md_insumos->presentacion(),
			'cant'=>8,
		    'numero'=>8,
		    'ini'=>0,
		    'pagina' => $pagina
		);
 		$this->load->view('insumos_user',$send);
    }
}

?>