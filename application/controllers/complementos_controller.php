<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Complementos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_complementos');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}
	function productos()
	{
		log_message('debug', 'md_complementos->producto($prod)');

			$prod = 'COA';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0
			);
 
				$this->load->view('complementos',$send);
		
		log_message('debug', 'md_complementos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}
	
	function paginas()
	{
		$ini=$_GET['ini'];
		$numero=$_GET['numero'];
		$cant=$_GET['cant'];
		$prod=$_GET['cat'];

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>$cant,
				 'numero'=>$numero,
				 'ini'=>$ini,
				 
			);

			
 
				$this->load->view('complementos',$send);
	}

	function filtro()
	{

		if ((!isset($_POST['marca']))&&(!isset($_POST['presentacion']))&&(!isset($_POST['pais']))&&(!isset($_POST['cate']))) {
			$marca="%";
			$presen="%";
			$pais="%";
			$prod="%";
		}
		else{
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=8;
		}
		if ($canti<1){
			$canti=8;
		}

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->filtro($marca,$presen,$pais,$prod),
				'pais'=>$this->md_complementos->pais(),
				'cate'=>$this->md_complementos->catego(),
				'marca'=>$this->md_complementos->marca(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0
			);

			
 
				$this->load->view('complementos',$send);


	}

}

?>