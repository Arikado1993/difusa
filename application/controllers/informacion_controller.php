<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informacion extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_informacion');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}

	function producto($id)
	{
		$send = array(
				'carousel_config' => $this->carousel->informacion(),
				'info'=>$this->md_informacion->prod($id)
			);

				$this->load->view('informacion',$send);
	}

}

?>