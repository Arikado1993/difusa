<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {	 
    
    function __construct(){
        parent::__construct();
        $this->load->library('grocery_CRUD');
		$this->grocery_crud->set_language("spanish");
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
		$this->load->model('md_pages');
		if (!$this->tank_auth->is_logged_in()) {
				redirect('/auth/login/');
		}
    }
    
    public function index(){
        $this->load->view('admin/simple');
    }
    
    
    public function _callback_image_collection($value,$row){
		$this->db->set_dbprefix('ops_');
		$filename = $this->md_pages->get_the_collection_thumbnail_src($row->id_category_collection);
		$img_alt = 'Colección';
		if (!$filename){
			$image_path = base_url().'content/uploads/images/categories_collection/default.png';
		}
		else{
			$image_path = base_url().'content/uploads/images/categories_collection/'.$filename[0]->meta_value;
		}
		return '<img height="80" src="'.$image_path.'" alt="'.$img_alt.'"/>';
    }
    
    function options(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Información');
		$where = "option_name = 'facebook' OR option_name = 'email_paypal' OR option_name = 'twitter'";
		$crud->where($where);
		$crud->set_table('ops_options');
		$fields = array(
			'option_name' => 'Opción',
			'option_value' => 'Valor'
		);
		foreach ($fields as $key => $value){
			$crud->display_as($key,$value);
		}
		$crud->required_fields('option_value');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->columns(array('option_name', 'option_value'));
		$crud->fields(array('option_value'));
		$output = $crud->render();
		$variable['title_form'] = 'Información';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function pages(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Páginas');
		$crud->set_table('ops_pages');
		$fields = array(
			'title' => 'Título',
			'content' => 'Contenido'
		);
		foreach ($fields as $key => $value){
			$crud->display_as($key,$value);
		}
		$crud->required_fields('content');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->columns(array('title', 'content'));
		$crud->fields(array('title', 'content', 'updated_at'));
		$crud->field_type('updated_at', 'hidden', $date);
		$output = $crud->render();
		$variable['title_form'] = 'Páginas';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function points(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Puntos');
		$crud->set_table('ops_rings_points');
		$crud->display_as('point','Punto');
		$crud->required_fields('point');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->columns(array('point'));
		$crud->fields(array('point', 'created_at'));
		$crud->field_type('created_at', 'hidden', $date);
		$output = $crud->render();
		$variable['title_form'] = 'Puntos';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function measures(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Medidas');
		$crud->set_table('ops_rings_measures');
		$crud->display_as('measure','Medida');
		$crud->required_fields('measure');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->columns(array('measure'));
		$crud->fields(array('measure', 'created_at'));
		$crud->field_type('created_at', 'hidden', $date);
		$output = $crud->render();
		$variable['title_form'] = 'Medidas';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function kilates(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Kilates');
		$crud->set_table('ops_rings_kilates');
		$crud->display_as('kilate','Kilate');
		$crud->required_fields('kilate');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->columns(array('kilate'));
		$crud->fields(array('kilate', 'created_at'));
		$crud->field_type('created_at', 'hidden', $date);
		$output = $crud->render();
		$variable['title_form'] = 'Kilate';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function colors_for_ring(){
		date_default_timezone_set('Mexico/General');
		$date = date("Y-m-d H:i:s");
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Color de oro');
		$crud->set_table('ops_rings_colors');
		$crud->display_as('color','Color');
		$crud->required_fields('color');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->columns(array('color'));
		$crud->fields(array('color', 'created_at'));
		$crud->field_type('created_at', 'hidden', $date);
		$output = $crud->render();
		$variable['title_form'] = 'Colores de Oro';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function banners(){
		$web_sections = $this->md_pages->get_web_sections();
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('banners');
		$crud->set_table('ops_banners');
		$fields = array(
			'url' => 'Enlace',
			'image' =>'Imagen',
			'id_web_section' => 'Página'
		);
		foreach ($fields as $key=>$value){
			$crud->display_as($key,$value);
		}
		$crud->field_type('id_web_section','dropdown', $web_sections);
		$crud->required_fields('image','id_web_section');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->set_field_upload('image','content/uploads/images/banners');
		$output = $crud->render();
		$variable['title_form'] = 'Banners';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    
	 function collections(){
		  date_default_timezone_set('Mexico/General');
		  $date = date("Y-m-d H:i:s");
		  $collections = array();
		  foreach($this->md_pages->get_all_collections() as $value){
			   $collections[$value->id_collection] = $value->name;
		  }
		  $this->db->set_dbprefix(null);
		  $crud = $this->grocery_crud;
		  $crud->set_subject('Colecciones');
		  $crud->set_table("ops_categories_collections");
		  $fields = array(
			   'image' => 'Imagen',
			   'name'=>'Nombre',
			   'slug' => 'URL',
			   'id_collection' => 'Colección'
		  );
		  foreach ($fields as $key => $value){
			   $crud->display_as($key,$value);
		  }
		  $crud->columns(array('image','name','id_collection'));
		  $crud->fields(array('image','name','slug','id_collection','created_at','updated_at'));
		  $crud->callback_after_insert(array($this, 'after_insert_a_collection'));
		  $crud->callback_after_update(array($this, 'after_update_a_collection'));
		  $crud->field_type('id_collection','dropdown', $collections);
		  $crud->set_field_upload('image','content/uploads/images/categories_collection');
		  $crud->field_type('created_at', 'hidden', $date);
		  $crud->field_type('updated_at', 'hidden', $date);
		  $crud->unset_print();
		  $crud->unset_export();
		  $crud->unset_read();
		  $output = $crud->render();
		  $variable = array(
			   'title_form' => 'Colecciones'
		  );
		  $output->variable = $variable;
		  $this->load->view('admin/simple.php',$output);
	 }
	 
	 function after_insert_a_collection($post_array,$primary_key){
		  $insert = array(
			   "id_category_collection" => $primary_key,
			   "id_collection" => $post_array['id_collection'],
			   "created_at" => date('Y-m-d H:i:s')
		  );
		  $this->db->insert('ops_rel_category_collection',$insert);
		  
		  $insert_img = array(
			   "id_category_collections" => $primary_key,
			   "meta_key" => 'img',
			   "meta_value" => $post_array['image']
		  );
		  $this->db->insert('ops_categories_collections_meta',$insert_img);
		  
		  return true;
	 }
	 
	 function after_update_a_collection($post_array,$primary_key){
		  
		  $this->db->delete('ops_rel_category_collection', array('id_category_collection' => $primary_key)); 
		  $update = array(
			   "id_category_collection" => $primary_key,
			   "id_collection" => $post_array['id_collection'],
			   "created_at" => date('Y-m-d H:i:s')
		  );
		  $this->db->insert('ops_rel_category_collection',$update);
		  
		  $this->db->delete('ops_categories_collections_meta', array('id_category_collections' => $primary_key));
		  $insert_img = array(
			   "id_category_collections" => $primary_key,
			   "meta_key" => 'img',
			   "meta_value" => $post_array['image']
		  );
		  $this->db->insert('ops_categories_collections_meta',$insert_img);
		  return true;
	 }
	
	public function view_invoicings($id){
		$payment = $this->md_pages->get_info_of_payment_by_id($id);
		if($payment){
			$send = array(
				'title_form' => '',
				'actived_view_invo' => true,
				'payment' => $payment
			);
			$this->load->view('admin/simple', $send);
		}
	}
	
    public function invoicings(){
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Pago');
		$crud->set_table("ops_pagos");
		$fields = array(
			'folio' => 'Folio',
			'pagado'=>'Pago Realizado',
			'metodo_pago'=>'Método de pago',
			'fecha_registro' => 'Fecha de registro'
		);
		foreach ($fields as $key => $value){
			$crud->display_as($key,$value);
			$show_fields[] = $key;
		}
		$crud->field_type('pagado','dropdown',
            array('0' => 'Pendiente', '1' => 'Pagado'));
		$crud->order_by('fecha_registro','desc');
		$crud->add_action('Ver', base_url().'content/themes/admin/images/btn_color_search.png', 'admin/view_invoicings');
		$crud->columns($show_fields);
		$crud->fields($show_fields);
		$crud->where('metodo_pago !=','ind');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_read();
		$crud->unset_delete();
		$output = $crud->render();
		$variable = array(
			'title_form' => 'Historial de pagos'
		);
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    public function precios_universales(){
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Precio Universal');
		$crud->set_table("ops_universal_prices");
		$fields = array(
			'price' => 'Precio',
			'model'=>'Modelo',
			'id_rings_kilate'=>'Kilate',
			'id_rings_point' => 'Punto'
		);
		foreach ($fields as $key => $value){
			$crud->display_as($key,$value);
			$show_fields[] = $key;
		}
		$crud->columns($show_fields);
		$crud->fields(array('price','model'));
		$crud->field_type('id_rings_kilate','multiselect', array( "1"  => "14K", "2" => "18K"));
		$puntos = array(
			'1' => '15',
			'2' => '20',
			'3' => '25',
			'4' => '30',
			'5' => '35',
			'6' => '40',
			'7' => '45',
			'8' => '50',
			'9' => '70',
			'10' => '100',
			'11' => '150',
			'12' => '200'
		);
		$crud->field_type('id_rings_point','multiselect', $puntos);
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_add();
		$crud->unset_delete();
		$output = $crud->render();
		$variable = array(
			'title_form' => 'Precios Universales'
		);
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
		
    }
    
    function products(){
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Producto');
		$crud->set_table("ops_products");
		$fields = array(
			'image' => 'Imagen',
			'name'=>'Nombre',
			'price'=>'Precio',
			'model' => 'Modelo'
		);
		foreach ($fields as $key => $value){
			$crud->display_as($key,$value);
			$show_fields[] = $key;
		}
		$crud->columns($show_fields);
		$crud->callback_column('image',array($this,'_callback_image_products'));
		$crud->add_action('Editar', base_url().'content/themes/admin/images/editar.png', 'products/edit');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_add();
		$output = $crud->render();
		$variable = array(
			'title_form' => 'Productos',
			'activate_add_product' => true
		);
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    public function _callback_image_products($value,$row){
		$this->db->set_dbprefix('ops_');
		$filename = $this->md_pages->get_the_product_thumbnail_src($row->id_product);
		$img_alt = 'Producto';
		if (!$filename){
			$image_path = base_url().'content/uploads/images/products/default.png';
		}
		else{
			$image_path = base_url().'content/uploads/images/products/'.$filename[0]->meta_value;
		}
		return '<img height="80" src="'.$image_path.'" alt="'.$img_alt.'"/>';
    }
    
    
    function users(){
		$this->db->set_dbprefix(null);
		$crud = $this->grocery_crud;
		$crud->set_subject('Usuario');
		$crud->set_table('ops_users');
		$fields = array(
			'username' => 'Usuario',
			'password' => 'Contraseña',
			'email' =>'Email',
			'activated' =>'Estado'
		);
		foreach ($fields as $key=>$value){
			$crud->display_as($key,$value);
		}
		$crud->columns(array('username','email','activated'));
		$crud->fields(array('username','password','email','activated'));
		$crud->required_fields(array('username','password','email','activated'));
		//$crud->field_type('password', 'password');
		$crud->callback_edit_field('password',array($this,'set_password_input_to_empty'));
		$crud->callback_add_field('password',array($this,'set_password_input_to_empty'));
		//$crud->field_type('id_profiles','multiselect', array( "1"  => "Administrador", "0" => "Cliente"));
		$crud->callback_before_update(array($this,'encrypt_password_callback_update'));
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		$output = $crud->render();
		$variable['title_form'] = 'Usuarios';
		$output->variable = $variable;
		$this->load->view('admin/simple.php',$output);
    }
    
    function set_password_input_to_empty(){
		return "<input type='password' name='password' value='' />";
    }
    
    function encrypt_password_callback_update($post_array, $primary_key){
		$hasher = new PasswordHash(
			$this->config->item('phpass_hash_strength', 'tank_auth'),
			$this->config->item('phpass_hash_portable', 'tank_auth')
		);
		$hashed_password = $hasher->HashPassword($post_array['password']);
		if(!empty($post_array['password'])){
			$post_array['password']=$hashed_password;
		}
		else{
			unset($post_array['password']);
		}
		return $post_array;
    }
    
    function encrypt_password_callback($post_array){
		$hasher = new PasswordHash(
			$this->config->item('phpass_hash_strength', 'tank_auth'),
			$this->config->item('phpass_hash_portable', 'tank_auth')
		);
		$hashed_password = $hasher->HashPassword($post_array['password']);
		$post_array['password']=$hashed_password;
		return $post_array;
    }
    
    
}