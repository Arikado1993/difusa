	<?php
	function updateSuscription($user,$insert)
    {
        $this->db->where('id_user',$user); //obtienes el id de usuario, que para tu caso, deberias de obener el id de la pregunta
        $this->db->delete('ops_subscriptions_ops_userss'); // condicional que uso para borrar las suscripciones previas del usuario
        $this->db->insert_batch('ops_subscriptions_ops_userss',$insert); // inserccion de datos de arreglos
    }


    function updateNews()
	{
		$idUser = $this->session->userdata('id_client'); // obtengo el id de usuario, para tu caso id de pregunta
		$suscripciones = $this->input->post('suscripciones',TRUE); // capturo todos los checkbox que hayan sido palomeados
		$generalNews = $this->panel_model->getNews();// obtengo todos los id de preguntas
		$insertar = array(); // arreglo que se mandara para inserccion en la tabla
		for($c = 0 ; $c<count($generalNews) ; $c++)
			{
				if(isset($suscripciones[$c])) /* esta condicional es para que solo al existir un valor dentro del arreglo de suscripciones realize la inserccion de ese valor de checkbox asociado al id de usuario (en tu caso de pregunta), todo esto dentro de un array
				*/
				{
					$insertar[$c] = array('id_user' => $idUser, // el id de usuario/pregunta al que se asociara el checkbox
								'id_subscription' => $suscripciones[$c] // la opcion seleccionada del arreglo de checkbox
						);
				}
			}
		if(count($insertar) < 1 )
		{
			$this->panel_model->noSuscription($idUser);			
		}
		else
		{			
			$this->panel_model->updateSuscription($idUser,$insertar);
		}	
		$this->news();
	}
	?>