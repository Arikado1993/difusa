<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pago extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_complementos');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}

	function exito()
	{
		$data="La compra será procesada.";
		$this->load->view('error',$data);
	}

	function error()
	{
		$data="Fallo en la compra.";
		$this->load->view('error',$data);
	}

}