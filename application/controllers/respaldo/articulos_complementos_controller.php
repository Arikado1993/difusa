<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articulos_complementos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_complementos');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}

	function productos()
	{
		log_message('debug', 'md_complementos->producto($prod)');
			$prod = 'COA';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0
			);
			$this->load->view('complementos_user',$send);
		log_message('debug', 'md_complementos->producto(INS).$this->db = '.print_r($this->db,TRUE));
	}
	
	function paginas()
	{
		$ini=$_GET['ini'];
		$numero=$_GET['numero'];
		$cant=$_GET['cant'];
		$prod=$_GET['cat'];

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>$cant,
				 'numero'=>$numero,
				 'ini'=>$ini,
			);
		$this->load->view('complementos_user',$send);
	}

	function filtro()
	{

		if ((!isset($_POST['marca']))&&(!isset($_POST['presentacion']))&&(!isset($_POST['pais']))&&(!isset($_POST['cate']))) {
			$marca="%";
			$presen="%";
			$pais="%";
			$prod="%";
		}
		else{
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=8;
		}
		if ($canti<1){
			$canti=8;
		}

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->filtro($marca,$presen,$pais,$prod),
				'pais'=>$this->md_complementos->pais(),
				'cate'=>$this->md_complementos->catego(),
				'marca'=>$this->md_complementos->marca(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0
			);

			
 				
				$this->load->view('complementos_user',$send);


	}


	function agregarProducto()
    {
    	
        $id = $_POST['idproduct'];
        $producto = $this->md_complementos->info($id);
        $cantidad = $_POST['cantidad'];
        $nombre = $_POST['nombre'];
        $precio2 = $_POST['precio'];
        $codigo = $_POST['codigo'];
        $moneda_origen = $_POST['moneda'];
		$moneda_destino='MXN';
        /*
		$get = file_get_contents("https://www.google.com/finance/converter?a=$precio2&from=$moneda_origen&to=$moneda_destino");
		$get = explode("<span class=bld>",$get);
		$get = explode("</span>",$get[1]);*/

     	$precio=$precio2;
     	//$precio=preg_replace("/[^0-9\.]/", null, $get[0]);
        //obtenemos el contenido del carrito
        $carrito = $this->cart->contents();
 
        foreach ($carrito as $item) 
        {
            //si el id del producto es igual que uno que ya tengamos
            //en la cesta le sumamos uno a la cantidad
            if ($item['id'] == $id) 
            {
                $cantidad = $cantidad + $item['qty'];
            }
        }
        //cogemos los productos en un array para insertarlos en el carrito
        $insert = array(
            'id' => $id,
            'qty' => $cantidad,
            'price' => $precio,
            'name' => $nombre
        );
        //si hay opciones creamos un array con las opciones y lo metemos
        //en el carrito
        if ($codigo) 
        {
            $insert['options'] = array(
            'code'=> $codigo
            );
        }
        //insertamos al carrito
        $this->cart->insert($insert);
        
        //redirigimos mostrando un mensaje con las sesiones flashdata
        //de codeigniter confirmando que hemos agregado el producto
        $this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
     
		$prod = 'COA';
		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_complementos->productos($prod),
				'pais'=>$this->md_complementos->pais(),
				'marca'=>$this->md_complementos->marca(),
				'cate'=>$this->md_complementos->catego(),
				'presentacion'=>$this->md_complementos->presentacion(),
				'cant'=>8,
				'numero'=>8,
				'ini'=>0
				);
		$this->load->view('complementos_user',$send);
    }
    function insertWish()
    {
    	$producto = $this->input->post('idproduct',TRUE);
    	$user = $this->session->userdata('id_client');
    	$wishInsert = array(
    		'id_user' => $user,
    		'id_product' => $this->input->post('idproduct',TRUE)
    	);
    	$this->md_insumos->insertWish($wishInsert);
    	echo "<script language='javascript'>alert('Producto agregado correctamente');</script>";
    	$prod = 'MAL';
		$send = array(
			'carousel_config' => $this->carousel->productos(),
			'info'=>$this->md_insumos->productos($prod),
			'pais'=>$this->md_insumos->pais(),
			'marca'=>$this->md_insumos->marca(),
			'cate'=>$this->md_insumos->catego(),
			'presentacion'=>$this->md_insumos->presentacion(),
			'cant'=>8,
		    'numero'=>8,
		    'ini'=>0
		);
 		$this->load->view('complementos_user',$send);
    }
}

?>