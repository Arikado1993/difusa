<?php
	function load_view($template_part = '', $params = array()){
		$CI =& get_instance();
		$t = get_option('template');
		if($template_part == ''){
			$template_part = 'index';
		}
		$CI->load->view("$t/$template_part", $params);
	}
	
	function get_header($template_part = '', $params = array()){
		$CI =& get_instance();
		$t = get_option('template');
		if($template_part != ''){
			$template_part = '-'.$template_part;
		}
		$CI->load->view("$t/header$template_part", $params);
	}

	function get_footer($template_part = '', $params = array()){
		$CI =& get_instance();
		$t = get_option('template');
		if($template_part != ''){
			$template_part = '-'.$template_part;
		}
		$CI->load->view("$t/footer$template_part", $params);
	}
	
	
	function site_title($title = ''){
		$CI = &get_instance();
		$tag_title = '';
		if (!isset($title))
			$title = '';
		if(is_home()){
			$tag_title = get_option('blogname'). ' | '.get_option('blogdescription');
		} else {
			//$tag_title = $CI->router->class.' '.$CI->router->method.' ';
			$tag_title = get_option('blogname'). ' | '.get_option('blogdescription');
			if($CI->router->method == 'aboutus'){
				$tag_title = 'Nosotros | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
			if($CI->router->method == 'rings'){
				$CI->load->model('md_pages');
				if($CI->uri->segment(2)){
					$category = $CI->md_pages->get_category_rings_by('slug',$CI->uri->segment(2));
					$tag_title = $category->name.' | '.get_option('blogname'). ' | '.get_option('blogdescription');
				}
				else{
					$tag_title = 'SOLITARIO ARRIBA DE MEDIO KILATE | '.get_option('blogname'). ' | '.get_option('blogdescription');
				}
			}
			if($CI->router->method == 'contact'){
				$tag_title = 'Contacto | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
			if($CI->router->method == 'know_your_measure'){
				$tag_title = '¿Quieres saber tu medida? | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
			if($CI->router->method == 'quality'){
				$tag_title = 'Calidad y Confianza | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
			if($CI->router->method == 'maintenance'){
				$tag_title = 'Mantenimiento de por vida | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
			if($CI->router->method == 'ring'){
				$CI->load->model('md_pages');
				$product = $CI->md_pages->get_product_by('slug',$CI->uri->segment(3));
				$tag_title = $product->name.' | '.$product->model.' | '.get_option('blogname'). ' | '.get_option('blogdescription');
			}
		}
		return $tag_title;
	}
	
	function pre($v, $c = '#000000'){
		echo "<pre style='color: $c; font-family: Consolas, Arial, Helvetica, sans-serif;'>";
		print_r($v);
		echo '</pre>';
	}
	function is_home(){
		$d = false;
		if(base_url() == current_url()){
			$d = true;
		}
		return $d;
	}
	
	function get_option($option = '', $default = false, $single = true){
		$CI =& get_instance();
		$CI->load->database();
		$CI->db->select('
			id_option,
			id_option `option_id`,
			option_name,
			option_name `name`,
			option_value,
			option_value `value`
		');
		$CI->db->where('option_name', $option);
		$CI->db->from('options');
		$d = $default;
		$q = $CI->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				if($single){
					$d = $row->option_value;
					break;
				} else {
					$d[] =  $row;
				}
			}
		}
		return $d;
	}
	
	function get_states(){
		$states = array(
			'Aguascalientes',
			'Baja California',
			'Baja California Sur',
			'Campeche',
			'Chiapas',
			'Chihuahua',
			'Coahuila',
			'Colima',
			'Distrito Federal',
			'Durango',
			'Estado de Mexico',
			'Guanajuato',
			'Guerrero',
			'Hidalgo',
			'Jalisco',
			'Michoacán',
			'Morelos',
			'Nayarit',
			'Nuevo León',
			'Oaxaca',
			'Puebla',
			'Querétaro',
			'Quintana Roo',
			'San Luis Potosí',
			'Sinaloa',
			'Sonora',
			'Tabasco',
			'Tamaulipas',
			'Tlaxcala',
			'Veracruz',
			'Yucatán',
			'Zacatecas'
		);
		return $states;
	}
function format_number($n = ''){
	if ($n == ''){
		return '';
	}
	$n = trim(preg_replace('/([^0-9\.])/i', '', $n));
	return number_format($n, 2, '.', ',');
}